<?php

namespace Kusiu\PortfolioBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction() {
        
        $projects = $this->getDoctrine()
                ->getRepository('PortfolioBundle:Project')
                ->findBy(
					array(), 
					array('date' => 'DESC')
				  );
       
        return $this->render(
            'PortfolioBundle:Default:index.html.twig',
            array ('projectList' => $projects)
        );
    }
    
    /**
     * Send Contact Form
     *
     * @return string
     */
    public function sendFormAction() {

        $result['error'] = true;
       
        if (isset($_POST['contactName'])
                && isset($_POST['comments'])
                && isset($_POST['email'])) {

            $message = \Swift_Message::newInstance()
                ->setSubject('New message from portfolio.kusiu.com - ' . $_POST['email'])
                ->setFrom("info@twojkurier.uk")
                ->setTo('kusiu84@gmail.com')
                ->setBody(
                    wordwrap($_POST['comments'], 70),
                    'text/html'
                )
            ;

            if ($this->get('mailer')->send($message)) {
                $result['error'] = false;
            }

        }

        return new Response(json_encode($result));
    }
}
